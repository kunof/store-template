/*
 * Third party
 */
//= ../../bower_components/modernizr/modernizr.js
//= ../../bower_components/jquery/dist/jquery.js
//= ../../bower_components/uikit/js/uikit.js


/*
 * Custom
 */
//= partials/app.js
//= partials/owl.carousel.js
//= partials/nouislider.min.js
//= partials/jquery.fancybox.js



$('.search i').click(function(){
    $(this).parent('.search').toggleClass('active');
    $(this).parent('.search').find('input').focus();
});
$('.cart-button').click(function(){
    $('.cart-popup').toggle();
});
$('.menu-clicker').click(function(){
    $('.main-menu').toggleClass('active');
});
$('.content, .search, .content-topped, .person-cab-button, .cart').click(function(){
    $('.main-menu').removeClass('active');
});
$('.content, .menu-centred, .search, .content-topped, .person-cab-button, .menu-clicker').click(function(){
    $('.cart-popup').hide();
});
$('.content, .menu-centred, .cart, .content-topped, .person-cab-button, .menu-clicker').click(function(){
    $('.search').removeClass('active');
});
$('.scroll-to').click(function(){
    var data = $(this).attr('data-scroll');
    var positon = $('.'+data).position();
    var headerHeight = $('header').height();
    $('body,html').animate({
        scrollTop: positon.top - headerHeight
    }, 600);
});
$('.owl-carousel').owlCarousel({
    items:4,
    loop:'true',
    nav:'true',
    margin:20
});
$('.slider-caruseul').owlCarousel({
    items:1,
    nav:'true',
    autoplay:'true'
});
$('.slides-wrap').owlCarousel({
    items:1,
    nav:'true',
    loop: 'true',
    autoplay:'true'
});
$('.previews-img').owlCarousel({
    items:4,
    nav:'true',
    margin:20
});
$('.products-menu .parent a').click(function(){
    var thisDom = $(this).parent();
    $('.parent').not(thisDom).removeClass('active').find('ul').slideUp();
    $(thisDom).find('ul').slideToggle();
    $(thisDom).toggleClass('active');
    return false;
});
$('.count div').click(function(){
    var value = $(this).parent().find('input').val();
    var value = parseFloat(value);
    if ($(this).hasClass('plus')){
        value++;
        $(this).parent().find('input').val(value);
    }else if ($(this).hasClass('minus')){
        if (value > 1){
            value = value - 1;
            $(this).parent().find('input').val(value);
        }
    }
});

//Галерея товара
var clonedImg = $('.product__images .owl-item').eq(0).find('.previews-img__element').html();
$('.product__images .main-img').html(clonedImg);
$('.product__images .owl-item').eq(0).find('.previews-img__element').addClass('active');

$('.product__images .previews-img__element').click(function(){
    $('.product__images .previews-img__element').removeClass('active');
    $(this).addClass('active');
    var src = $(this).find('img').attr('src');
    $('.product__images .main-img img').attr('src', src);
});

//Вкладки товара
$('.product-tabs__buttons > div').click(function(){
    if ($(this).hasClass('active')){}else{
        var id = $(this).attr('class');
        $('.product-tabs__buttons > div').removeClass('active');
        $(this).addClass('active');
        $('.product-tabs__tab').removeClass('active');
        $('#'+id).addClass('active');
    }
});

//Фильтр - Категория
$('.switches-left h2').click(function(){
    $('.switches-left h2').removeClass('active');
    $(this).addClass('active');
    var id = $(this).attr('id');
    if (id == 'fil'){
        $('.products-menu').hide();
        $('.filter-wrap').show();
    }
    if (id == 'cat'){
        $('.products-menu').show();
        $('.filter-wrap').hide();
    }
});
$('.filter-wrap li>span').click(function(){
    $(this).siblings('.param-filter').slideToggle();
});


//Фильтр - Категория
$('.switches-left h2').click(function(){
    if ($(this).hasClass('mobile-active')){
        $('.products-menu').removeClass('mobile-show').addClass('mobile-hide');
        $('.filter-wrap').removeClass('mobile-show').addClass('mobile-hide');
        $(this).removeClass('mobile-active');
    }else{
        $('.switches-left h2').removeClass('mobile-active');
        $(this).addClass('mobile-active');
        var id = $(this).attr('id');
        if (id == 'fil'){
            $('.products-menu').removeClass('mobile-show').addClass('mobile-hide');
            $('.filter-wrap').addClass('mobile-show').removeClass('mobile-hide');
        }
        if (id == 'cat'){
            $('.products-menu').addClass('mobile-show').removeClass('mobile-hide');
            $('.filter-wrap').removeClass('mobile-show').addClass('mobile-hide');
        }
    }
    
});


 //Price slider
var slider = document.getElementById('slider');
if (slider) {
    noUiSlider.create(slider, {
        start: [parseInt($('#value-span').val()), parseInt($('#value-input').val())],
        connect: true,
        step: 100,
        range: {
            'min': 0,
            'max': 10000
        }
    });
    var valueInput = document.getElementById('value-input'),
        valueSpan = document.getElementById('value-span');

    slider.noUiSlider.on('update', function (values, handle) {
        if (handle) {
            valueInput.value = values[handle];
        } else {
            valueSpan.value = values[handle];
        }
    });

    valueInput.addEventListener('change', function () {
        slider.noUiSlider.set([null, this.value]);
    });
    valueSpan.addEventListener('change', function () {
        slider.noUiSlider.set([this.value, null]);
    });
    slider.noUiSlider.on('change', function(){
        catalogFilterChange();
    });
}



//select
$('.select').click(function(){
     $('.select').not(this).removeClass('open').find('.sel').slideUp(100);
     $(this).toggleClass('open').find('.sel').slideToggle(100);
 });
 $('.select').find('.sel span').click(function(){
     var value = $(this).text();
     $(this).parents('.select').find('.default').text(value);
     $(this).parents('.select').find('.default').data('id', $(this).data('id') || 0);
 });

$('.two-buttons>div').click(function(){
    var id = $(this).attr('id');
    $('.two-buttons > div').removeClass('active');
    $(this).addClass('active');
    $('.search-content').removeClass('active');
    $('.' + id).addClass('active');
});

$('.buttonShowdInfo').click(function(){
    var thisEl = $(this).parents('tr');
    if ($(this).hasClass('active')){
        $(this).text('Подробнее');
        $(this).removeClass('active');
    }else{
        $(this).text('Скрыть');
        $(this).addClass('active');
    }
    $(thisEl).find('.hided').toggle();
    $(thisEl).find('.showed').toggle();
});




//Fancybox
function show_popup(href, effectIn) {
    effectIn = effectIn || "bounceIn";

    $.fancybox({
        autoWidth: true,
        autoHeight: true,
        fitToView: true,
        scrolling: true,
        autoSize: true,
        closeClick: false,
        openEffect: effectIn,
        closeEffect: effectIn,
        closeBtn: false,
        wrapCSS: 'form',
        padding: 0,
        openSpeed: 0,
        closeSpeed: 0,
        href: href
    });

    var $w = $(".fancybox-wrap");

    $w.addClass("animated " + effectIn);
    $w.one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function () {
        $w.removeClass("animated " + effectIn);
    });
}

$("body").on("click", ".popup-link", function () {
    var $t = $(this),
        href = $t.attr("data-href"),
        $w,
        effectIn = "bounceIn";
    show_popup(href, effectIn);
    return false;
});
$('body').on('click', ".close-modal, .overlay", function () {
    $.fancybox.close(); 
    return false;
});
$('.to-register').click(function(){
    $('#auth').slideUp(300);
    $('#register').slideDown(300);
    $('.to-register').hide();
    $('.to-login').show();
});
$('.to-login').click(function(){
    $('#auth').slideDown(300);
    $('#register').slideUp(300);
    $('.to-register').show();
    $('.to-login').hide();
});
$('.to-password').click(function(){
    $('#password').slideDown(300);
    $('#register').slideUp(300);
    $('#auth').slideUp(300);
    $('.to-register').hide();
    $('.to-login').hide();
    $('.bottom-modal').slideUp(300);
});
$('.person-cab-button').click(function(){
    $('#auth').show();
    $('#register').hide();
    $('#password').hide();
    $('.bottom-modal').show();
    $('.to-register').show();
    $('.to-login').hide();
});

$('.content-left__inner>h2').click(function(){
    $(this).toggleClass('active');
});